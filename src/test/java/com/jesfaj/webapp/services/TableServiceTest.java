package com.jesfaj.webapp.services;

import com.jesfaj.webapp.WebApp;
import com.jesfaj.webapp.controller.dto.TableDTO;
import com.jesfaj.webapp.controller.mapper.TableMapper;
import com.jesfaj.webapp.domain.Restaurant;
import com.jesfaj.webapp.domain.Table;
import com.jesfaj.webapp.exception.FoundException;
import com.jesfaj.webapp.exception.NotFoundException;
import com.jesfaj.webapp.exception.ServiceException;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import static org.mockito.Mockito.when;

@RunWith(SpringJUnit4ClassRunner.class)
@SpringBootTest(classes = WebApp.class)
public class TableServiceTest {

    @Mock
    private TableService tableService;

    @Test
    public void testGetTableServiceException() {
        when(tableService.findById(null)).thenThrow(ServiceException.class);
    }

    @Test
    public void testGetTableNotFoundException() {
        TableDTO tableDTO = new TableDTO();
        tableDTO.setId(0);
        when(tableService.findById(tableDTO)).thenThrow(NotFoundException.class);
    }

    @Test
    public void testGetTableFoundWithoutTables() {
        TableDTO tableDTOSearch = new TableDTO();
        TableDTO tableDTOFound = new TableDTO();
        tableDTOSearch.setId(1);

        tableDTOFound.setId(1);
        tableDTOFound.setCapacidad(8);
        tableDTOFound.setIdRestaurant(1);

        when(tableService.findById(tableDTOSearch)).thenReturn(tableDTOFound);
    }

    @Test
    public void testSetTableServiceException() {
        when(tableService.save(null)).thenThrow(ServiceException.class);
    }

    @Test
    public void testSetTableWithoutRestaurant() {
        Table table = new Table();
        table.setCapacidad(8);
        table.setRestaurant(null);

        TableDTO tableDTOReturned = new TableDTO();
        tableDTOReturned.setId(1);
        tableDTOReturned.setCapacidad(8);
        tableDTOReturned.setIdRestaurant(1);
        TableDTO tableDTO = TableMapper.makeTableDTO(table);
        when(tableService.save(tableDTO)).thenReturn(tableDTOReturned);
    }

    @Test
    public void testSetTableWithRestaurant() {
        Restaurant restaurant = new Restaurant();
        restaurant.setId(1);

        Table table = new Table();
        table.setCapacidad(8);
        table.setRestaurant(restaurant);

        TableDTO tableDTOReturned = new TableDTO();
        tableDTOReturned.setId(1);
        tableDTOReturned.setCapacidad(8);
        tableDTOReturned.setIdRestaurant(1);
        TableDTO tableDTO = TableMapper.makeTableDTO(table);
        when(tableService.save(tableDTO)).thenReturn(tableDTOReturned);
    }

//    @Test
//    public void testSetTableWhitoutTablesExists() {
//        Table table = new Table();
//        table.setId(1);
//        table.setNombre("Test(JUNIT)");
//        table.setDireccion("Test(JUNIT)");
//        table.setDescripcion("Test(JUNIT)");
//        table.setTables(null);
//
//        TableDTO tableDTO = TableMapper.makeTableDTO(table);
//        when(tableService.save(tableDTO)).thenThrow(FoundException.class);
//    }
}

