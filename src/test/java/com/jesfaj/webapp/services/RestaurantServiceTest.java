package com.jesfaj.webapp.services;

import com.jesfaj.webapp.WebApp;
import com.jesfaj.webapp.controller.dto.RestaurantDTO;
import com.jesfaj.webapp.controller.mapper.RestaurantMapper;
import com.jesfaj.webapp.domain.Restaurant;
import com.jesfaj.webapp.exception.FoundException;
import com.jesfaj.webapp.exception.NotFoundException;
import com.jesfaj.webapp.exception.ServiceException;

import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import static org.mockito.Mockito.when;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.springframework.boot.test.context.SpringBootTest;

@RunWith(SpringJUnit4ClassRunner.class)
@SpringBootTest(classes = WebApp.class)
public class RestaurantServiceTest {

    @Mock
    private RestaurantService restaurantService;

    @Test
    public void testGetRestaurantNotFoundException() {
        RestaurantDTO restaurantDTO = new RestaurantDTO();
        restaurantDTO.setId(0);
        when(restaurantService.findById(restaurantDTO)).thenThrow(NotFoundException.class);
    }

    @Test
    public void testGetRestaurantFoundWithoutTables() {
        RestaurantDTO restaurantDTOSearch = new RestaurantDTO();
        RestaurantDTO restaurantDTOFound = new RestaurantDTO();
        restaurantDTOSearch.setId(1);

        restaurantDTOFound.setId(1);
        restaurantDTOFound.setNombre("Test(JUNIT)");
        restaurantDTOFound.setDescripcion("Test(JUNIT)");
        restaurantDTOFound.setDireccion("Test(JUNIT)");
        restaurantDTOFound.setIdTables(null);

        when(restaurantService.findById(restaurantDTOSearch)).thenReturn(restaurantDTOFound);
    }

    @Test
    public void testSetRestaurantServiceException() {
        when(restaurantService.save(null)).thenThrow(ServiceException.class);
    }

    @Test
    public void testSetRestaurantWithoutTables() {
        Restaurant restaurant = new Restaurant();
        restaurant.setNombre("Test(JUNIT)");
        restaurant.setDireccion("Test(JUNIT)");
        restaurant.setDescripcion("Test(JUNIT)");
        restaurant.setTables(null);

        RestaurantDTO restaurantDTOReturned = new RestaurantDTO();
        restaurantDTOReturned.setId(1);
        restaurantDTOReturned.setNombre("Test(JUNIT)");
        restaurantDTOReturned.setDescripcion("Test(JUNIT)");
        restaurantDTOReturned.setDireccion("Test(JUNIT)");
        restaurantDTOReturned.setIdTables(null);
        RestaurantDTO restaurantDTO = RestaurantMapper.makeRestaurantDTO(restaurant);
        when(restaurantService.save(restaurantDTO)).thenReturn(restaurantDTOReturned);
    }

    @Test
    public void testSetRestaurantWithoutTablesExists() {
        Restaurant restaurant = new Restaurant();
        restaurant.setId(1);
        restaurant.setNombre("Test(JUNIT)");
        restaurant.setDireccion("Test(JUNIT)");
        restaurant.setDescripcion("Test(JUNIT)");
        restaurant.setTables(null);

        RestaurantDTO restaurantDTO = RestaurantMapper.makeRestaurantDTO(restaurant);
        when(restaurantService.save(restaurantDTO)).thenThrow(FoundException.class);
    }
}

