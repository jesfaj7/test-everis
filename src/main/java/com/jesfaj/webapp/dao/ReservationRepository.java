package com.jesfaj.webapp.dao;

import com.jesfaj.webapp.domain.Reservation;
import org.springframework.data.repository.CrudRepository;

public interface ReservationRepository extends CrudRepository<Reservation, Integer> {}