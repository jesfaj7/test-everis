package com.jesfaj.webapp.dao;

import com.jesfaj.webapp.domain.Restaurant;
import org.springframework.data.repository.CrudRepository;

public interface RestaurantRepository extends CrudRepository<Restaurant, Integer> {
}