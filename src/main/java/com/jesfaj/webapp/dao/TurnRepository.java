package com.jesfaj.webapp.dao;

import com.jesfaj.webapp.domain.Turn;
import org.springframework.data.repository.CrudRepository;

public interface TurnRepository extends CrudRepository<Turn, Integer> {}