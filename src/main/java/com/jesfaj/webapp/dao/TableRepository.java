package com.jesfaj.webapp.dao;

import com.jesfaj.webapp.domain.Table;
import org.springframework.data.repository.CrudRepository;

public interface TableRepository extends CrudRepository<Table, Integer> {}