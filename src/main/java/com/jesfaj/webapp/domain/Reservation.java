package com.jesfaj.webapp.domain;

import com.fasterxml.jackson.annotation.JsonIgnore;
import javax.persistence.*;

@Entity
@javax.persistence.Table(name = "T_Reservation")
public class Reservation {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;
    @Column(name = "localizador")
    private Integer localizador;
    @Column(name = "personas")
    private Integer personas;
    @Column(name = "dia")
    private String dia;

    @ManyToOne
    @JoinColumn(name = "table_id")
	@JsonIgnore
	private Table table;

    @ManyToOne
	@JsonIgnore
	@JoinColumn(name = "turno_id")
    private Turn turn;

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public Integer getLocalizador() {
		return localizador;
	}

	public void setLocalizador(Integer localizador) {
		this.localizador = localizador;
	}

	public Integer getPersonas() {
		return personas;
	}

	public void setPersonas(Integer personas) {
		this.personas = personas;
	}

	public String getDia() {
		return dia;
	}

	public void setDia(String dia) {
		this.dia = dia;
	}


	public Turn getTurn() {
		return this.turn;
	}

	public void setTurn(Turn turn) {
		this.turn = turn;
	}

	public Table getTable() {
		return table;
	}

	public void setTable(Table table) {
		this.table = table;
	}

	@Override
	public String toString() {
		return "Reservation{" +
				"id=" + id +
				", localizador=" + localizador +
				", personas=" + personas +
				", dia='" + dia + '\'' +
				", table=" + table +
				", turn=" + turn +
				'}';
	}
}
