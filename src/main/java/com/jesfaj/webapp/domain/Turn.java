package com.jesfaj.webapp.domain;

import javax.persistence.*;
import java.util.List;

@Entity
@javax.persistence.Table(name = "T_Turn")
public class Turn {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;
    @Column(name = "descripcion")
    private String descripcion;
    
    @OneToMany(mappedBy = "turn", cascade = CascadeType.REMOVE, fetch= FetchType.LAZY)
	private List<Reservation> reservations;
    
	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getDescripcion() {
		return descripcion;
	}

	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}

	public List<Reservation> getReservations() {
		return reservations;
	}

	public void setReservations(List<Reservation> reservations) {
		this.reservations = reservations;
	}

	@Override
	public String toString() {
		return "Turn{" +
				"id=" + id +
				", descripcion='" + descripcion + '\'' +
				", reservations=" + reservations +
				'}';
	}
}
