package com.jesfaj.webapp.domain;

import com.fasterxml.jackson.annotation.JsonIgnore;

import javax.persistence.*;
import java.util.List;

@Entity
@javax.persistence.Table(name = "T_Table")
public class Table {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;
    @Column(name = "capacidad")
    private Integer capacidad;
    
    @ManyToOne(fetch=FetchType.EAGER)
    @JoinColumn(name = "restaurante_id")
	@JsonIgnore
	private Restaurant restaurant;

	@OneToMany(mappedBy = "table", cascade = CascadeType.REMOVE, fetch= FetchType.LAZY)
	private List<Reservation> reservations;

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public Integer getCapacidad() {
		return capacidad;
	}

	public void setCapacidad(Integer capacidad) {
		this.capacidad = capacidad;
	}

	public Restaurant getRestaurant() {
		return this.restaurant;
	}

	public void setRestaurant(Restaurant restaurant) {
		this.restaurant = restaurant;
	}

	@Override
	public String toString() {
		return "Table [id=" + id + ", capacidad=" + capacidad + ", restaurant=" + restaurant + "]";
	}

    
}
