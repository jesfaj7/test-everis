package com.jesfaj.webapp.domain;

import java.util.List;
import javax.persistence.*;

@Entity
@javax.persistence.Table(name = "T_Restaurant")
public class Restaurant {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;
    @Column(name = "nombre")
    private String nombre;
    @Column(name = "descripcion")
    private String descripcion;
    @Column(name = "direccion")
    private String direccion;

    @OneToMany(mappedBy = "restaurant", cascade = CascadeType.REMOVE, fetch= FetchType.LAZY)
    private List<Table> tables;
    
	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public String getDescripcion() {
		return descripcion;
	}

	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}

	public String getDireccion() {
		return direccion;
	}

	public void setDireccion(String direccion) {
		this.direccion = direccion;
	}

	public List<Table> getTables() {
		return tables;
	}

	public void setTables(List<Table> tables) {
		this.tables = tables;
	}

	@Override
	public String toString() {
		return "Restaurant [id=" + id + ", nombre=" + nombre + ", descripcion=" + descripcion + ", direccion="
				+ direccion + ", tables=" + tables + "]";
	}
        
}
