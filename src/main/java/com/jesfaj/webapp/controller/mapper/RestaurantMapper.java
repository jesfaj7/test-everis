package com.jesfaj.webapp.controller.mapper;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.List;
import java.util.stream.Collectors;

import com.jesfaj.webapp.domain.Restaurant;
import com.jesfaj.webapp.controller.dto.RestaurantDTO;
import com.jesfaj.webapp.domain.Table;
;

public class RestaurantMapper {

    public static Restaurant makeRestaurant(RestaurantDTO restaurantDTO) {
    	Restaurant restaurant = new Restaurant();
    	if(restaurantDTO.getId() == 0) {
    		restaurant.setId(null);
    	}else {
    		restaurant.setId(restaurantDTO.getId());
    	}

        List<Table> tables = new ArrayList<>();
        Table table;
        if(restaurantDTO.getIdTables() != null){
            for (Integer item: restaurantDTO.getIdTables()) {
                table = new Table();
                table.setId(item);
                tables.add(table);
            }
        }else{
            tables = null;
        }
    	restaurant.setTables(tables);
        restaurant.setNombre(restaurantDTO.getNombre());
    	restaurant.setDescripcion(restaurantDTO.getDescripcion());
    	restaurant.setDireccion(restaurantDTO.getDireccion());
        return restaurant;
    }

    public static RestaurantDTO makeRestaurantDTO(Restaurant restaurant) {
        RestaurantDTO restaurantDTO = new RestaurantDTO();
        restaurantDTO.setId(restaurant.getId());
        restaurantDTO.setNombre(restaurant.getNombre());
        restaurantDTO.setDescripcion(restaurant.getDescripcion());
        restaurantDTO.setDireccion(restaurant.getDireccion());
        List<Integer> idTables = new ArrayList<>();
        if(restaurant.getTables() != null){
            for (Table item: restaurant.getTables()) {
                idTables.add(item.getId());
            }
        }else{
            idTables = null;
        }
        restaurantDTO.setIdTables(idTables);
        return restaurantDTO;
    }

    public static List<RestaurantDTO> makeRestaurantDTOList(Collection<Restaurant> restaurants) {
        return restaurants.stream().map(RestaurantMapper::makeRestaurantDTO).collect(Collectors.toList());
    }

    public static List<Restaurant> makeRestaurantList(Collection<RestaurantDTO> restaurantsDTO) {
        if (restaurantsDTO == null) {
            return Collections.emptyList();
        }
        return restaurantsDTO.stream()
                .map(RestaurantMapper::makeRestaurant)
                .collect(Collectors.toList());
    }
}
