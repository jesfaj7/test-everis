package com.jesfaj.webapp.controller.mapper;

import java.util.Collection;
import java.util.Collections;
import java.util.List;
import java.util.stream.Collectors;

import com.jesfaj.webapp.controller.dto.TableDTO;
import com.jesfaj.webapp.domain.Restaurant;
import com.jesfaj.webapp.domain.Table;

public class TableMapper {
    public static Table makeTable(TableDTO tableDTO) {
    	Table table = new Table();
    	if(tableDTO.getId() == 0) {
    		table.setId(null);
    	}else {
    		table.setId(tableDTO.getId());
    	}
        Restaurant restaurant = new Restaurant();
    	restaurant.setId(tableDTO.getIdRestaurant());
        table.setRestaurant(restaurant);
        table.setCapacidad(tableDTO.getCapacidad());
        return table;
    }

    public static TableDTO makeTableDTO(Table table) {
        TableDTO tableDTO = new TableDTO();
        tableDTO.setId(table.getId());
        tableDTO.setCapacidad(table.getCapacidad());
        if(table.getRestaurant() != null){
            tableDTO.setIdRestaurant(table.getRestaurant().getId());
        }
        return tableDTO;
    }

    public static List<TableDTO> makeTableDTOList(Collection<Table> tables) {
        return tables.stream()
                .map(TableMapper::makeTableDTO)
                .collect(Collectors.toList());
    }

    public static List<Table> makeTableList(Collection<TableDTO> tablesDTO) {
        if (tablesDTO == null) {
            return Collections.emptyList();
        }
        return tablesDTO.stream()
                .map(TableMapper::makeTable)
                .collect(Collectors.toList());
    }
}
