package com.jesfaj.webapp.controller.mapper;

import java.util.Collection;
import java.util.Collections;
import java.util.List;
import java.util.stream.Collectors;

import com.jesfaj.webapp.controller.dto.ReservationDTO;
import com.jesfaj.webapp.domain.Reservation;
import com.jesfaj.webapp.domain.Table;
import com.jesfaj.webapp.domain.Turn;

public class ReservationMapper {
    public static Reservation makeReservation(ReservationDTO reservationDTO) {
    	Reservation reservation = new Reservation();
    	if(reservationDTO.getId() == 0) {
    		reservation.setId(null);
    	}else {
    		reservation.setId(reservationDTO.getId());
    	}
    	Table table = new Table();
    	table.setId(reservationDTO.getIdTable());
        reservation.setTable(table);
        Turn turn = new Turn();
        turn.setId(reservationDTO.getIdTurn());
        reservation.setTurn(turn);
        reservation.setLocalizador(reservationDTO.getLocalizador());
    	reservation.setPersonas(reservationDTO.getPersonas());
    	reservation.setDia(reservationDTO.getDia());
        return reservation;
    }

    public static ReservationDTO makeReservationDTO(Reservation reservation) {
        ReservationDTO reservationDTO = new ReservationDTO();
        reservationDTO.setId(reservation.getId());
        reservationDTO.setIdTable(reservation.getTable().getId());
        reservationDTO.setIdTurn(reservation.getTurn().getId());
        reservationDTO.setLocalizador(reservation.getLocalizador());
        reservationDTO.setPersonas(reservation.getPersonas());
        reservationDTO.setDia(reservation.getDia());
        return reservationDTO;
    }

    public static List<ReservationDTO> makeReservationDTOList(Collection<Reservation> reservations) {
        return reservations.stream()
                .map(ReservationMapper::makeReservationDTO)
                .collect(Collectors.toList());
    }

    public static List<Reservation> makeReservationList(Collection<ReservationDTO> reservationsDTO) {
        if (reservationsDTO == null) {
            return Collections.emptyList();
        }
        return reservationsDTO.stream()
                .map(ReservationMapper::makeReservation)
                .collect(Collectors.toList());
    }

}
