package com.jesfaj.webapp.controller.mapper;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.List;
import java.util.stream.Collectors;

import com.jesfaj.webapp.controller.dto.TurnDTO;
import com.jesfaj.webapp.domain.Reservation;
import com.jesfaj.webapp.domain.Turn;

public class TurnMapper {
    public static Turn makeTurn(TurnDTO turnDTO) {
    	Turn turn = new Turn();
    	if(turnDTO.getId() == 0) {
    		turn.setId(null);
    	}else {
    		turn.setId(turnDTO.getId());
    	}

        List<Reservation> reservations = new ArrayList<>();
    	Reservation reservation;
    	if(turnDTO.getIdReservations() != null){
            for (Integer item: turnDTO.getIdReservations()) {
                reservation = new Reservation();
                reservation.setId(item);
                reservations.add(reservation);
            }
        }else{
            reservations = null;
        }
    	turn.setReservations(reservations);
    	turn.setDescripcion(turnDTO.getDescripcion());
        return turn;
    }

    public static TurnDTO makeTurnDTO(Turn turn) {
        TurnDTO turnDTO = new TurnDTO();
        turnDTO.setId(turn.getId());
        turnDTO.setDescripcion(turn.getDescripcion());
        List<Integer> idReservations = new ArrayList<>();
        if(turn.getReservations() != null){
            for (Reservation item: turn.getReservations()) {
                idReservations.add(item.getId());
            }
        }else{
            idReservations = null;
        }
        turnDTO.setIdReservations(idReservations);
        return turnDTO;
    }

    public static List<TurnDTO> makeTurnDTOList(Collection<Turn> turns) {
        return turns.stream()
                .map(TurnMapper::makeTurnDTO)
                .collect(Collectors.toList());
    }

    public static List<Turn> makeTurnList(Collection<TurnDTO> turnsDTO) {
        if (turnsDTO == null) {
            return Collections.emptyList();
        }
        return turnsDTO.stream()
                .map(TurnMapper::makeTurn)
                .collect(Collectors.toList());
    }

}
