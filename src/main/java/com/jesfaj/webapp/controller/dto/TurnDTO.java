package com.jesfaj.webapp.controller.dto;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import java.util.List;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({"id", "descripcion", "idReservations"})
public class TurnDTO {
    private Integer id;
    private String descripcion;
    private List<Integer> idReservations;

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getDescripcion() {
		return descripcion;
	}

	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}

	public List<Integer> getIdReservations() {
		return idReservations;
	}

	public void setIdReservations(List<Integer> idReservations) {
		this.idReservations = idReservations;
	}
}
