package com.jesfaj.webapp.controller.dto;

import java.util.List;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({"id", "nombre", "descripcion", "direccion", "idTables"})
public class RestaurantDTO {
    private Integer id;
    private String nombre;
    private String descripcion;
    private String direccion;
    private List<Integer> idTables;

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public String getDescripcion() {
		return descripcion;
	}

	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}

	public String getDireccion() {
		return direccion;
	}

	public void setDireccion(String direccion) {
		this.direccion = direccion;
	}

	public List<Integer> getIdTables() {
		return idTables;
	}

	public void setIdTables(List<Integer> idTables) {
		this.idTables = idTables;
	}
}
