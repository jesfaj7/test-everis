package com.jesfaj.webapp.controller.dto;


import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({"id", "localizador", "personas", "dia", "idTable", "idTurn"})
public class ReservationDTO {

	private Integer id;
    private Integer localizador;
    private Integer personas;
    private String dia;
    private Integer idTable;
    private Integer idTurn;

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public Integer getLocalizador() {
		return localizador;
	}

	public void setLocalizador(Integer localizador) {
		this.localizador = localizador;
	}

	public Integer getPersonas() {
		return personas;
	}

	public void setPersonas(Integer personas) {
		this.personas = personas;
	}

	public String getDia() {
		return dia;
	}

	public void setDia(String dia) {
		this.dia = dia;
	}

	public Integer getIdTable() {
		return idTable;
	}

	public void setIdTable(Integer idTable) {
		this.idTable = idTable;
	}

	public Integer getIdTurn() {
		return idTurn;
	}

	public void setIdTurn(Integer idTurn) {
		this.idTurn = idTurn;
	}
}
