package com.jesfaj.webapp.controller.impl;

import com.jesfaj.webapp.controller.ReservationsController;
import com.jesfaj.webapp.controller.dto.DataDTO;
import com.jesfaj.webapp.controller.dto.ReservationDTO;
import com.jesfaj.webapp.services.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;
import javax.validation.Valid;
import java.util.List;
import static com.jesfaj.webapp.util.ControllerConstants.GENERAL_MAPPING_CONTROLLER_URL;

@RestController
@RequestMapping(value = GENERAL_MAPPING_CONTROLLER_URL)
public class ReservationsControllerImpl implements ReservationsController {
	
	@Autowired
	ReservationService reservationService;

	@GetMapping(value = "/reservations")
	public DataDTO<List<ReservationDTO>> getAll() {
		DataDTO<List<ReservationDTO>> listDataDTO = new DataDTO<>();
		listDataDTO.setData(reservationService.findAll());
		return listDataDTO;
	}

	@GetMapping(value = "/reservations/{id}")
	public @ResponseBody DataDTO<ReservationDTO> getById(@Valid @PathVariable Integer id) {
		DataDTO<ReservationDTO> dataDTO = new DataDTO<>();
		dataDTO.setData(reservationService.findById(id));
		return dataDTO;
	}

	@DeleteMapping("/reservations/{id}")
	public @ResponseBody void remove(@Valid @PathVariable Integer id) {
		ReservationDTO reservationDTO = new ReservationDTO();
		reservationDTO.setId(id);
		reservationService.delete(reservationDTO);
	}

	@PatchMapping("/reservations")
	public @ResponseBody
	DataDTO modify(@Valid @RequestBody ReservationDTO reservationDTO) {
		return new DataDTO(reservationService.update(reservationDTO));
	}

	@PostMapping(value = "/reservations")
	@ResponseStatus(HttpStatus.CREATED)
	public @ResponseBody
	DataDTO set(@Valid @RequestBody ReservationDTO reservationDTO) {
		return new DataDTO(reservationService.save(reservationDTO));
	}

//	@PostMapping(value = "/reservations")
//	@ResponseStatus(HttpStatus.CREATED)
//	public @ResponseBody
//	DataDTO<List<ReservationDTO>> setMult(@Valid @RequestBody List<ReservationDTO> reservationDTOList) {
//		return new DataDTO(reservationService.saveMult(reservationDTOList));
//	}
}
