package com.jesfaj.webapp.controller.impl;

import com.jesfaj.webapp.controller.SwaggerController;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import springfox.documentation.annotations.ApiIgnore;

@ApiIgnore
@Controller
public class SwaggerControllerImpl implements SwaggerController {

    @RequestMapping("/swagger")
    public String home() {
        return "redirect:swagger-ui.html";
    }

}
