package com.jesfaj.webapp.controller.impl;

import com.jesfaj.webapp.controller.InitController;
import com.jesfaj.webapp.services.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import com.jesfaj.webapp.exception.DaoException;
import com.jesfaj.webapp.exception.ServiceException;
import com.jesfaj.webapp.services.ReservationService;

@RestController
@RequestMapping(value = "scrumbar/v1")
public class InitControllerImpl implements InitController {
	
	@Autowired
	RestaurantService restaurantService;

	@Autowired
	TableService tableService;

	@Autowired
	TurnService turnService;

	@Autowired
	ReservationService reservationService;

	//Only for test the use of DB
	@GetMapping(value = "/init")
	public String init() throws ServiceException, DaoException {

//		Restaurant restaurant = new Restaurant("Nombre TEST", "Descripcion TEST", "Direccion TEST", null, null);
//		restaurant = restaurantService.saveOrUpdate(restaurant);
//		Table table = new Table(4, restaurant);
//		tableService.saveOrUpdate(table);
//		Turn turn = new Turn("Turn TEST", null);
//		turnService.saveOrUpdate(turn);
//		Reservation reservation = new Reservation(1, 4, "20/02/2019", restaurant, turn);
//		reservationService.saveOrUpdate(reservation);
//
//		System.out.println("===********Restaurantes********===");
//		System.out.println(restaurantService.findAll());
//		System.out.println("===********Mesas********===");
//		System.out.println(tableService.findAll());
//		System.out.println("===********Turnos********===");
//		System.out.println(turnService.findAll());
//		System.out.println("===********Reservas********===");
//		System.out.println(reservationService.findAll());

		return "init";
	}
}
