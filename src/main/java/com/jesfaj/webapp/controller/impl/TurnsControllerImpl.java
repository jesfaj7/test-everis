package com.jesfaj.webapp.controller.impl;

import com.jesfaj.webapp.controller.TurnsController;
import com.jesfaj.webapp.controller.dto.DataDTO;
import com.jesfaj.webapp.controller.dto.TurnDTO;
import com.jesfaj.webapp.services.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;
import javax.validation.Valid;
import java.util.List;
import static com.jesfaj.webapp.util.ControllerConstants.GENERAL_MAPPING_CONTROLLER_URL;

@RestController
@RequestMapping(value = GENERAL_MAPPING_CONTROLLER_URL)
public class TurnsControllerImpl implements TurnsController {

    @Autowired
    TurnService turnService;

    @GetMapping(value = "/turns")
    public DataDTO<List<TurnDTO>> getAll() {
        return new DataDTO(turnService.findAll());
    }

    @PostMapping(value = "/turns")
    @ResponseStatus(HttpStatus.CREATED)
    public @ResponseBody
    DataDTO<TurnDTO> set(@Valid @RequestBody TurnDTO turnDTO) {
        return new DataDTO(turnService.save(turnDTO));
    }

}
