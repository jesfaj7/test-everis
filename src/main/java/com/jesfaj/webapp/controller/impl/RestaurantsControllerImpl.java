package com.jesfaj.webapp.controller.impl;

import com.jesfaj.webapp.controller.RestaurantsController;
import com.jesfaj.webapp.controller.dto.DataDTO;
import com.jesfaj.webapp.controller.dto.RestaurantDTO;
import com.jesfaj.webapp.services.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;
import javax.validation.Valid;
import java.util.List;

import static com.jesfaj.webapp.util.ControllerConstants.GENERAL_MAPPING_CONTROLLER_URL;

@RestController
@RequestMapping(value = GENERAL_MAPPING_CONTROLLER_URL)
public class RestaurantsControllerImpl implements RestaurantsController {

    @Autowired
    RestaurantService restaurantService;

    @PostMapping(value = "/restaurants")
    @ResponseStatus(HttpStatus.CREATED)
    public @ResponseBody
    DataDTO<RestaurantDTO> set(@Valid @RequestBody RestaurantDTO restaurantDTO) {
        return new DataDTO(restaurantService.save(restaurantDTO));
    }

    @GetMapping(value = "/restaurants")
    public DataDTO<List<RestaurantDTO>> getAll() {
        return new DataDTO(restaurantService.findAll());
    }

}
