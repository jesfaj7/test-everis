package com.jesfaj.webapp.controller.impl;

import com.jesfaj.webapp.controller.HomeController;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import springfox.documentation.annotations.ApiIgnore;

@ApiIgnore
@Controller
public class HomeControllerImpl implements HomeController {

    @RequestMapping("/")
    public String index() {
        return "index";
    }

}
