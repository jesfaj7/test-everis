package com.jesfaj.webapp.controller.impl;

import com.jesfaj.webapp.controller.TablesController;
import com.jesfaj.webapp.controller.dto.DataDTO;
import com.jesfaj.webapp.controller.dto.TableDTO;
import com.jesfaj.webapp.services.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;
import javax.validation.Valid;
import java.util.List;
import static com.jesfaj.webapp.util.ControllerConstants.GENERAL_MAPPING_CONTROLLER_URL;

@RestController
@RequestMapping(value = GENERAL_MAPPING_CONTROLLER_URL)
public class TablesControllerImpl implements TablesController {

	@Autowired
	TableService tableService;

	@PostMapping(value = "/tables")
	@ResponseStatus(HttpStatus.CREATED)
	public @ResponseBody
	DataDTO<TableDTO> set(@Valid @RequestBody TableDTO tableDTO) {
		return new DataDTO(tableService.save(tableDTO));
	}

	@GetMapping(value = "/tables")
	public DataDTO<List<TableDTO>> getAll(){
		return new DataDTO(tableService.findAll());
	}
}
