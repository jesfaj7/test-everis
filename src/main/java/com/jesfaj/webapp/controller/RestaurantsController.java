package com.jesfaj.webapp.controller;

import com.jesfaj.webapp.controller.dto.DataDTO;
import com.jesfaj.webapp.controller.dto.RestaurantDTO;
import com.jesfaj.webapp.exception.DaoException;
import com.jesfaj.webapp.exception.FoundException;
import com.jesfaj.webapp.exception.ServiceException;
import java.util.List;

public interface RestaurantsController {
    DataDTO<RestaurantDTO> set(RestaurantDTO restaurantDTO);
    DataDTO<List<RestaurantDTO>> getAll();
}
