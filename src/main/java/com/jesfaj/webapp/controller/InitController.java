package com.jesfaj.webapp.controller;

import com.jesfaj.webapp.exception.DaoException;
import com.jesfaj.webapp.exception.ServiceException;

public interface InitController {
    String init() throws ServiceException, DaoException;
}
