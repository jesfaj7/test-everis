package com.jesfaj.webapp.controller.handler;

import com.jesfaj.webapp.controller.dto.DataDTO;
import com.jesfaj.webapp.controller.dto.ErrorDTO;
import com.jesfaj.webapp.exception.DaoException;
import com.jesfaj.webapp.exception.FoundException;
import com.jesfaj.webapp.exception.NotFoundException;
import com.jesfaj.webapp.exception.ServiceException;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestControllerAdvice;
import org.springframework.web.servlet.config.annotation.EnableWebMvc;

import javax.servlet.http.HttpServletResponse;

@EnableWebMvc
@RestControllerAdvice
public class GlobalControllerExceptionHandler {

    @ExceptionHandler(value = {ServiceException.class})
//    @ResponseStatus(value = HttpStatus.BAD_REQUEST, reason = "Some constraints are violated(SERVICE)")
    @ResponseBody
    public DataDTO handleServiceException(ServiceException e, HttpServletResponse response) {
        ErrorDTO errorDTO = new ErrorDTO();
        errorDTO.setCode(Integer.toString(HttpStatus.BAD_REQUEST.value()) + "-Service");
        errorDTO.setMessage(HttpStatus.BAD_REQUEST.getReasonPhrase());
        DataDTO dataDTO = new DataDTO();
        dataDTO.setData(errorDTO);
        return dataDTO;
    }

    @ExceptionHandler(value = {DaoException.class})
//    @ResponseStatus(value = HttpStatus.BAD_REQUEST, reason = "Some constraints are violated(DAO)")
    @ResponseBody
    public DataDTO handleDaoException(DaoException e, HttpServletResponse response) {
        ErrorDTO errorDTO = new ErrorDTO();
        errorDTO.setCode(Integer.toString(HttpStatus.BAD_REQUEST.value()) + "-Dao");
        errorDTO.setMessage(HttpStatus.BAD_REQUEST.getReasonPhrase());
        DataDTO dataDTO = new DataDTO();
        dataDTO.setData(errorDTO);
        return dataDTO;
    }

    @ExceptionHandler(value = {NotFoundException.class})
//    @ResponseStatus(value = HttpStatus.NOT_FOUND, reason = "Could not find entity with id.")
    @ResponseBody
    public DataDTO handleNotFoundException(NotFoundException e, HttpServletResponse response) {
        ErrorDTO errorDTO = new ErrorDTO();
        errorDTO.setCode(Integer.toString(HttpStatus.NOT_FOUND.value()) + "-Not Found");
        errorDTO.setMessage(HttpStatus.NOT_FOUND.getReasonPhrase());
        DataDTO dataDTO = new DataDTO();
        dataDTO.setData(errorDTO);
        return dataDTO;
    }

    @ExceptionHandler(value = {FoundException.class})
//    @ResponseStatus(value = HttpStatus.FOUND, reason = "The entity exist.")
    @ResponseBody
    public DataDTO handleFoundException(FoundException e, HttpServletResponse response) {
        ErrorDTO errorDTO = new ErrorDTO();
        errorDTO.setCode(Integer.toString(HttpStatus.FOUND.value()) + "-Found");
        errorDTO.setMessage(HttpStatus.FOUND.getReasonPhrase());
        DataDTO dataDTO = new DataDTO();
        dataDTO.setData(errorDTO);
        return dataDTO;
    }

    @ExceptionHandler(value = Exception.class)
//    @ResponseStatus(value = HttpStatus.BAD_REQUEST, reason = "Some constraints are violated(GENERAL)")
    @ResponseBody
    public DataDTO handleException(Exception e) {
        ErrorDTO errorDTO = new ErrorDTO();
        errorDTO.setCode(Integer.toString(HttpStatus.BAD_REQUEST.value()) + "-General Exception");
        errorDTO.setMessage(HttpStatus.BAD_REQUEST.getReasonPhrase());
        DataDTO dataDTO = new DataDTO();
        dataDTO.setData(errorDTO);
        return dataDTO;
    }
}
