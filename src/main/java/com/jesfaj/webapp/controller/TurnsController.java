package com.jesfaj.webapp.controller;

import com.jesfaj.webapp.controller.dto.DataDTO;
import com.jesfaj.webapp.controller.dto.TurnDTO;
import com.jesfaj.webapp.exception.DaoException;
import com.jesfaj.webapp.exception.FoundException;
import com.jesfaj.webapp.exception.ServiceException;
import java.util.List;

public interface TurnsController {
    DataDTO<List<TurnDTO>> getAll();
    DataDTO<TurnDTO> set(TurnDTO turnDTO);
}
