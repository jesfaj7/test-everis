package com.jesfaj.webapp.controller;

import com.jesfaj.webapp.controller.dto.DataDTO;
import com.jesfaj.webapp.controller.dto.TableDTO;
import com.jesfaj.webapp.exception.DaoException;
import com.jesfaj.webapp.exception.FoundException;
import com.jesfaj.webapp.exception.ServiceException;
import java.util.List;

public interface TablesController {
    DataDTO<TableDTO> set(TableDTO tableDTO);
    DataDTO<List<TableDTO>> getAll();
}
