package com.jesfaj.webapp.controller;

import com.jesfaj.webapp.controller.dto.DataDTO;
import com.jesfaj.webapp.controller.dto.ReservationDTO;
import com.jesfaj.webapp.exception.DaoException;
import com.jesfaj.webapp.exception.NotFoundException;
import com.jesfaj.webapp.exception.ServiceException;
import java.util.List;

public interface ReservationsController {
    DataDTO<List<ReservationDTO>> getAll();
    DataDTO<ReservationDTO> getById(Integer id);
    void remove(Integer id);
    DataDTO modify(ReservationDTO reservationDTO);
    DataDTO set(ReservationDTO reservationDTO);
//    DataDTO<List<ReservationDTO>> setMult(List<ReservationDTO> reservationDTOList);
}
