package com.jesfaj.webapp.services;

import java.util.List;

import com.jesfaj.webapp.controller.dto.ReservationDTO;
import com.jesfaj.webapp.exception.DaoException;
import com.jesfaj.webapp.exception.FoundException;
import com.jesfaj.webapp.exception.NotFoundException;
import com.jesfaj.webapp.exception.ServiceException;

public interface ReservationService {

	ReservationDTO save(ReservationDTO reservationDTO) throws ServiceException, FoundException;

    ReservationDTO update(ReservationDTO reservationDTO) throws ServiceException, NotFoundException;

    void delete(ReservationDTO reservationDTO) throws ServiceException, NotFoundException;

    ReservationDTO findById(Integer id) throws ServiceException, DaoException, NotFoundException;
    
    List<ReservationDTO> findAll() throws ServiceException, DaoException;

    List<ReservationDTO> saveMult(List<ReservationDTO> reservationDTOList);
}
