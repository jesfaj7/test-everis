package com.jesfaj.webapp.services;

import java.util.List;

import com.jesfaj.webapp.controller.dto.TurnDTO;
import com.jesfaj.webapp.exception.DaoException;
import com.jesfaj.webapp.exception.FoundException;
import com.jesfaj.webapp.exception.NotFoundException;
import com.jesfaj.webapp.exception.ServiceException;

public interface TurnService {

	TurnDTO save(TurnDTO turnDTO) throws ServiceException, FoundException;

    TurnDTO update(TurnDTO turnDTO) throws ServiceException, NotFoundException;

    void delete(TurnDTO turnDTO) throws ServiceException, NotFoundException;

    TurnDTO findById(TurnDTO turnDTO) throws ServiceException, DaoException, NotFoundException;
    
    List<TurnDTO> findAll() throws ServiceException, DaoException;


}
