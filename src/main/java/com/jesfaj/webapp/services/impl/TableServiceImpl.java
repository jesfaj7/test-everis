package com.jesfaj.webapp.services.impl;

import java.util.List;
import java.util.stream.Collectors;

import com.jesfaj.webapp.controller.dto.TableDTO;
import com.jesfaj.webapp.controller.mapper.TableMapper;
import com.jesfaj.webapp.dao.TableRepository;
import com.jesfaj.webapp.domain.Table;
import com.jesfaj.webapp.exception.FoundException;
import com.jesfaj.webapp.exception.NotFoundException;
import com.jesfaj.webapp.exception.ServiceException;
import com.jesfaj.webapp.services.TableService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.persistence.EntityNotFoundException;

@Service("tableService")
public class TableServiceImpl implements TableService {

    private static final Logger logger = LoggerFactory.getLogger(TableServiceImpl.class);

    @Autowired
    private TableRepository tableRepository;

    public TableDTO save(TableDTO tableDTO) throws ServiceException, FoundException {
        if (tableDTO == null) {
            throw new ServiceException();
        }
        if(tableRepository.existsById(tableDTO.getId())){
            throw new FoundException();
        }
        Table table = TableMapper.makeTable(tableDTO);
        return TableMapper.makeTableDTO(tableRepository.save(table));
    }

    public TableDTO update(TableDTO tableDTO) throws ServiceException, NotFoundException {
        if (tableDTO == null) {
            throw new ServiceException();
        }
        if(!tableRepository.existsById(tableDTO.getId())){
            throw new NotFoundException();
        }
        Table table = TableMapper.makeTable(tableDTO);
        return TableMapper.makeTableDTO(tableRepository.save(table));
    }

    public void delete(TableDTO tableDTO) throws ServiceException, NotFoundException {
        if (tableDTO == null) {
            throw new ServiceException();
        }
        Table table = TableMapper.makeTable(tableDTO);
        tableRepository.delete(table);
    }

    public TableDTO findById(TableDTO tableDTO) throws ServiceException, NotFoundException {
        if (tableDTO == null) {
            throw new ServiceException();
        }
        Table table = tableRepository.findById(tableDTO.getId()).orElseThrow(() -> new EntityNotFoundException());
        tableDTO = TableMapper.makeTableDTO(table);
        return tableDTO;
    }
    
    public List<TableDTO> findAll() {
        List<Table> custList = (List<Table>)(Object)tableRepository.findAll();
        return custList.stream().map(b -> TableMapper.makeTableDTO(b)).collect(Collectors.toList());

//		List<RestaurantDTO> custList = restaurantRepository.findAll().stream().map(b -> RestaurantMapper.makeRestaurantDTO(b)).collect(Collectors.toList());
//		ObjectMapper mapper = new ObjectMapper();
//		String result = mapper.writeValueAsString(custList);

//        List<Table> tableList =  (List<Table>)tableRepository.findAll();
//        List<TableDTO> tableDTOList = new ArrayList<>();
//        for (Table item : tableList) {
//            TableDTO restaurantDTO = TableMapper.makeTableDTO(item);
//            tableDTOList.add(restaurantDTO);
//        }
//        return tableDTOList;
    }

	public static Logger getLogger() {
		return logger;
	}

}
