package com.jesfaj.webapp.services.impl;

import java.util.List;
import java.util.stream.Collectors;

import com.jesfaj.webapp.controller.dto.RestaurantDTO;
import com.jesfaj.webapp.controller.mapper.RestaurantMapper;
import com.jesfaj.webapp.dao.RestaurantRepository;
import com.jesfaj.webapp.domain.Restaurant;
import com.jesfaj.webapp.exception.FoundException;
import com.jesfaj.webapp.exception.NotFoundException;
import com.jesfaj.webapp.exception.ServiceException;
import com.jesfaj.webapp.services.RestaurantService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.persistence.EntityNotFoundException;

@Service("restaurantService")
public class RestaurantServiceImpl implements RestaurantService {

    private static final Logger logger = LoggerFactory.getLogger(RestaurantServiceImpl.class);

    @Autowired
    private RestaurantRepository restaurantRepository;

    public RestaurantDTO save(RestaurantDTO turnDTO) throws ServiceException, FoundException {
        if (turnDTO == null) {
            throw new ServiceException();
        }
        if(restaurantRepository.existsById(turnDTO.getId())){
            throw new FoundException();
        }
        Restaurant turn = RestaurantMapper.makeRestaurant(turnDTO);
        return RestaurantMapper.makeRestaurantDTO(restaurantRepository.save(turn));
    }

    public RestaurantDTO update(RestaurantDTO turnDTO) throws ServiceException, NotFoundException {
        if (turnDTO == null) {
            throw new ServiceException();
        }
        if(!restaurantRepository.existsById(turnDTO.getId())){
            throw new NotFoundException();
        }
        Restaurant turn = RestaurantMapper.makeRestaurant(turnDTO);
        return RestaurantMapper.makeRestaurantDTO(restaurantRepository.save(turn));
    }

    public void delete(RestaurantDTO restaurantDTO) throws ServiceException, NotFoundException {
        if (restaurantDTO == null) {
            throw new ServiceException();
        }
        Restaurant restaurant = RestaurantMapper.makeRestaurant(restaurantDTO);
        restaurantRepository.delete(restaurant);
    }

    public RestaurantDTO findById(RestaurantDTO restaurantDTO) throws ServiceException, NotFoundException{
        if (restaurantDTO == null) {
            throw new ServiceException();
        }

        Restaurant restaurant = restaurantRepository.findById(restaurantDTO.getId()).orElseThrow(() -> new NotFoundException());
        restaurantDTO = RestaurantMapper.makeRestaurantDTO(restaurant);
        return restaurantDTO;
    }

    public List<RestaurantDTO> findAll(){
        List<Restaurant> custList = (List<Restaurant>)(Object)restaurantRepository.findAll();
        return custList.stream().map(b -> RestaurantMapper.makeRestaurantDTO(b)).collect(Collectors.toList());

//		List<RestaurantDTO> custList = restaurantRepository.findAll().stream().map(b -> RestaurantMapper.makeRestaurantDTO(b)).collect(Collectors.toList());
//		ObjectMapper mapper = new ObjectMapper();
//		String result = mapper.writeValueAsString(custList);

//        List<Restaurant> restaurantList =  (List<Restaurant>)restaurantRepository.findAll();
//        List<RestaurantDTO> restaurantDTOList = new ArrayList<>();
//        for (Restaurant itemRestaurant : restaurantList) {
//            RestaurantDTO restaurantDTO = RestaurantMapper.makeRestaurantDTO(itemRestaurant);
//            restaurantDTOList.add(restaurantDTO);
//        }
//        return restaurantDTOList;
    }

	public static Logger getLogger() {
		return logger;
	}

}
