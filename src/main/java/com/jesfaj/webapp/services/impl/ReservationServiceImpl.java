package com.jesfaj.webapp.services.impl;

import java.util.List;
import java.util.stream.Collectors;
import com.jesfaj.webapp.controller.dto.ReservationDTO;
import com.jesfaj.webapp.controller.mapper.ReservationMapper;
import com.jesfaj.webapp.dao.ReservationRepository;
import com.jesfaj.webapp.domain.Reservation;
import com.jesfaj.webapp.exception.DaoException;
import com.jesfaj.webapp.exception.FoundException;
import com.jesfaj.webapp.exception.NotFoundException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.jesfaj.webapp.exception.ServiceException;
import com.jesfaj.webapp.services.ReservationService;

@Service("reservationService")
public class ReservationServiceImpl implements ReservationService {

    private static final Logger logger = LoggerFactory.getLogger(ReservationServiceImpl.class);

    @Autowired
    private ReservationRepository reservationRepository;

    public ReservationDTO save(ReservationDTO reservationDTO) throws ServiceException, FoundException {
        if (reservationDTO == null) {
            throw new ServiceException();
        }
        if(reservationRepository.existsById(reservationDTO.getId())){
            throw new FoundException();
        }
        Reservation reservation = ReservationMapper.makeReservation(reservationDTO);
        return ReservationMapper.makeReservationDTO(reservationRepository.save(reservation));
    }

    @Override
    public List<ReservationDTO> saveMult(List<ReservationDTO> reservationDTOList) {
        if (reservationDTOList == null) {
            throw new ServiceException();
        }

        List<Reservation> reservationList = ReservationMapper.makeReservationList(reservationDTOList);
        reservationDTOList = ReservationMapper.makeReservationDTOList((List<Reservation>)reservationRepository.saveAll(reservationList));
        return reservationDTOList;
    }

    public ReservationDTO update(ReservationDTO reservationDTO) throws ServiceException, NotFoundException {
        if (reservationDTO == null) {
            throw new ServiceException();
        }
        if(!reservationRepository.existsById(reservationDTO.getId())){
            throw new NotFoundException();
        }
        Reservation reservation = ReservationMapper.makeReservation(reservationDTO);
        return ReservationMapper.makeReservationDTO(reservationRepository.save(reservation));
    }

    public void delete(ReservationDTO reservationDTO) throws ServiceException, NotFoundException {
        if (reservationDTO == null) {
            throw new ServiceException();
        }
        if(!reservationRepository.existsById(reservationDTO.getId())){
            throw new NotFoundException();
        }
        Reservation reservation = ReservationMapper.makeReservation(reservationDTO);
        reservationRepository.delete(reservation);
    }

    public ReservationDTO findById(Integer id) throws ServiceException, DaoException, NotFoundException {
        if (id == null) {
            throw new ServiceException();
        }
        Reservation reservation = reservationRepository.findById(id).orElseThrow(() -> new NotFoundException());
        ReservationDTO reservationDTO = ReservationMapper.makeReservationDTO(reservation);
        return reservationDTO;
    }
    
    public List<ReservationDTO> findAll() throws ServiceException, DaoException {
		List<Reservation> custList = (List<Reservation>)(Object)reservationRepository.findAll();
        return custList.stream().map(b -> ReservationMapper.makeReservationDTO(b)).collect(Collectors.toList());

//		List<ReservationDTO> custList = reservationRepository.findAll().stream().map(b -> ReservationMapper.makeReservationDTO(b)).collect(Collectors.toList());
//		ObjectMapper mapper = new ObjectMapper();
//		String result = mapper.writeValueAsString(custList);

//        List<Reservation> reservationList =  (List<Reservation>)reservationRepository.findAll();
//        List<ReservationDTO> reservationDTOList = new ArrayList<>();
//        for (Reservation itemReservation : reservationList) {
//            ReservationDTO reservationDTO = ReservationMapper.makeReservationDTO(itemReservation);
//            reservationDTOList.add(reservationDTO);
//        }
//        return reservationDTOList;
    }

    public static Logger getLogger() {
		return logger;
	}

}
