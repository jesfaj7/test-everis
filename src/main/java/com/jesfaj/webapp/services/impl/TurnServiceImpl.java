package com.jesfaj.webapp.services.impl;

import java.util.List;
import java.util.stream.Collectors;

import com.jesfaj.webapp.controller.dto.TurnDTO;
import com.jesfaj.webapp.controller.mapper.TurnMapper;
import com.jesfaj.webapp.dao.TurnRepository;
import com.jesfaj.webapp.domain.Turn;
import com.jesfaj.webapp.exception.DaoException;
import com.jesfaj.webapp.exception.FoundException;
import com.jesfaj.webapp.exception.NotFoundException;
import com.jesfaj.webapp.exception.ServiceException;
import com.jesfaj.webapp.services.TurnService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service("turnService")
public class TurnServiceImpl implements TurnService {

    private static final Logger logger = LoggerFactory.getLogger(TurnServiceImpl.class);

    @Autowired
    private TurnRepository turnRepository;

    public TurnDTO save(TurnDTO turnDTO) throws ServiceException, FoundException {
        if (turnDTO == null) {
            throw new ServiceException();
        }
        if(turnRepository.existsById(turnDTO.getId())){
            throw new FoundException();
        }
        Turn turn = TurnMapper.makeTurn(turnDTO);
        return TurnMapper.makeTurnDTO(turnRepository.save(turn));
    }

    public TurnDTO update(TurnDTO turnDTO) throws ServiceException, NotFoundException {
        if (turnDTO == null) {
            throw new ServiceException();
        }
        if(!turnRepository.existsById(turnDTO.getId())){
            throw new NotFoundException();
        }
        Turn turn = TurnMapper.makeTurn(turnDTO);
        return TurnMapper.makeTurnDTO(turnRepository.save(turn));
    }

    public void delete(TurnDTO turnDTO) throws ServiceException, NotFoundException {
        if (turnDTO == null) {
            throw new ServiceException();
        }
        Turn turn = TurnMapper.makeTurn(turnDTO);
        turnRepository.delete(turn);
    }

    public TurnDTO findById(TurnDTO turnDTO) throws ServiceException, DaoException, NotFoundException {
        if (turnDTO == null) {
            throw new ServiceException();
        }
        Turn turn = turnRepository.findById(turnDTO.getId()).orElseThrow(() -> new NotFoundException());
        turnDTO = TurnMapper.makeTurnDTO(turn);
        return turnDTO;
    }
    
    public List<TurnDTO> findAll() throws ServiceException, DaoException {
        List<Turn> custList = (List<Turn>)(Object)turnRepository.findAll();
        return custList.stream().map(b -> TurnMapper.makeTurnDTO(b)).collect(Collectors.toList());

//		List<ReservationDTO> custList = reservationRepository.findAll().stream().map(b -> ReservationMapper.makeReservationDTO(b)).collect(Collectors.toList());
//		ObjectMapper mapper = new ObjectMapper();
//		String result = mapper.writeValueAsString(custList);

//        List<Turn> turnList =  (List<Turn>)turnRepository.findAll();
//        List<TurnDTO> turnDTOList = new ArrayList<>();
//        for (Turn item : turnList) {
//            TurnDTO turnDTO = TurnMapper.makeTurnDTO(item);
//            turnDTOList.add(turnDTO);
//        }
//
//        return turnDTOList;
        
    }

	public static Logger getLogger() {
		return logger;
	}

}
