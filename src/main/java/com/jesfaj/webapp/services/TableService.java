package com.jesfaj.webapp.services;

import java.util.List;

import com.jesfaj.webapp.controller.dto.TableDTO;
import com.jesfaj.webapp.exception.FoundException;
import com.jesfaj.webapp.exception.NotFoundException;
import com.jesfaj.webapp.exception.ServiceException;

public interface TableService {

    TableDTO save(TableDTO tableDTO) throws ServiceException, FoundException;

    TableDTO findById(TableDTO tableDTO) throws ServiceException, NotFoundException;
    
    List<TableDTO> findAll();

    TableDTO update(TableDTO tableDTO) throws ServiceException, NotFoundException;

    void delete(TableDTO tableDTO) throws ServiceException, NotFoundException;

}
