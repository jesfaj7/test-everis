package com.jesfaj.webapp.services;

import java.util.List;

import com.jesfaj.webapp.controller.dto.RestaurantDTO;
import com.jesfaj.webapp.exception.DaoException;
import com.jesfaj.webapp.exception.FoundException;
import com.jesfaj.webapp.exception.NotFoundException;
import com.jesfaj.webapp.exception.ServiceException;

public interface RestaurantService {

	RestaurantDTO save(RestaurantDTO restaurantDTO) throws ServiceException, FoundException;

    RestaurantDTO findById(RestaurantDTO restaurantDTO) throws ServiceException, NotFoundException;

    List<RestaurantDTO> findAll();

    RestaurantDTO update(RestaurantDTO restaurantDTO) throws ServiceException, NotFoundException;

    void delete(RestaurantDTO restaurantDTO) throws ServiceException, NotFoundException;

}
