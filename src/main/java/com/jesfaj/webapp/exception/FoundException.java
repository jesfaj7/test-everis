package com.jesfaj.webapp.exception;

public class FoundException extends RuntimeException {

    private static final long serialVersionUID = 1L;

    public FoundException() {
    }

    public FoundException(String message) {
        super(message);
    }

}

