package com.jesfaj.webapp.exception;

public class DaoException extends RuntimeException {

	private static final long serialVersionUID = 1L;

	public DaoException() {
    }

    public DaoException(String message) {
        super(message);
    }

}
