-- CREATE Script for init of DB

-- Create Tables Using DDL Query
--CREATE TABLE `restaurant` (
--  `id` int(11) NOT NULL AUTO_INCREMENT,
--  `descripcion` varchar(255) DEFAULT NULL,
--  `direccion` varchar(255) DEFAULT NULL,
--  `nombre` varchar(255) DEFAULT NULL,
--  PRIMARY KEY (`id`)
--) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--CREATE TABLE `turn` (
--  `id` int(11) NOT NULL AUTO_INCREMENT,
--  `descripcion` varchar(255) DEFAULT NULL,
--  PRIMARY KEY (`id`)
--) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--CREATE TABLE `table` (
--  `id` int(11) NOT NULL AUTO_INCREMENT,
--  `capacidad` int(11) DEFAULT NULL,
--  `restaurante_id_rest` int(11) DEFAULT NULL,
--  PRIMARY KEY (`id`),
--  FOREIGN KEY (`restaurante_id_rest`) REFERENCES restaurant(id)
--) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--CREATE TABLE `reservation` (
--  `id` int(11) NOT NULL AUTO_INCREMENT,
--  `dia` varchar(255) DEFAULT NULL,
--  `localizador` int(11) DEFAULT NULL,
--  `personas` int(11) DEFAULT NULL,
--  `restaurante_id_rest` int(11) DEFAULT NULL,
--  `restaurante_id_turno` int(11) DEFAULT NULL,
--  PRIMARY KEY (`id`),
--  FOREIGN KEY (`restaurante_id_rest`) REFERENCES restaurant(id),
--  FOREIGN KEY (`restaurante_id_turno`) REFERENCES turn(id)
--) ENGINE=InnoDB DEFAULT CHARSET=latin1;


-- INSERT Turnos
--insert into Turn ("descripcion") values ('TURNO_01_01');
--insert into Turn ("descripcion") values ('TURNO_02_01');
--insert into Turn ("descripcion") values ('TURNO_03_01');

-- INSERT Mesas
--insert into Table ("capacidad", "Restaurante_idRest") values (2, 1);
--insert into Table ("capacidad", "Restaurante_idRest") values (4, 1);
--insert into Table ("capacidad", "Restaurante_idRest") values (6, 1);

